provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/YamadaTerraformExecutionRole"
  }
}

module "parameter_store" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/parameter_store?ref=v0.2.1"

  parameters = [
    {
      name   = "/Yamada/Epona/App/Config/nablarch_db_url"
      value  = var.nablarch_db_url
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/nablarch_db_user"
      value  = var.nablarch_db_user
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/nablarch_db_password"
      value  = var.nablarch_db_password
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/nablarch_db_schema"
      value  = var.nablarch_db_schema
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/websocket_url"
      value  = var.websocket_url
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/mail_smtp_host"
      value  = var.mail_smtp_host
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/mail_smtp_port"
      value  = var.mail_smtp_port
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/mail_smtp_user"
      value  = var.mail_smtp_user
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/mail_smtp_password"
      value  = var.mail_smtp_password
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/mail_from_address"
      value  = var.mail_from_address
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/mail_returnpath"
      value  = var.mail_returnpath
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/application_external_url"
      value  = var.application_external_url
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/cors_origins"
      value  = var.cors_origins
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/nablarch_sessionstorehandler_cookiesecure"
      value  = var.nablarch_sessionstorehandler_cookiesecure
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
    {
      name   = "/Yamada/Epona/App/Config/nablarch_lettuce_simple_url"
      value  = var.nablarch_lettuce_simple_url
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/yamada-common-encryption-key"].key_alias_name
    },
  ]

  tags = {
    Owner              = "yamada"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
