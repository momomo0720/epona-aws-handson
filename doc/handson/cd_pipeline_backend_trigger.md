# バックエンドイベントトリガーの構築

バックエンドイベントトリガーを整えるためには `cd_pipeline_backend_trigger` patternを使っていきます[^1]。

[^1]: GitLabのロゴは[GitLab](https://about.gitlab.com/)によって制作されたものであり、[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)の下に提供されています。

![cd_pipeline_backend_trigger](../resources/cd_pipeline_backend_trigger.jpg)

## アジェンダ

[進め方について](../../README.md#進め方について)に記載があるように、講師の指示のもと、サンプルコードを参照しつつ、受講者の手でコードを書き適用していきます。

1. [cd_pipeline_backend_trigger patternについて](#cd_pipeline_backend_trigger-patternについて)
   1. [依存しているパターン](#依存しているパターン)
   1. [適用後の構成図](#適用後の構成図)
1. [事前準備](#事前準備)
1. [トリガー構築](#トリガー構築)
   1. [backend](#backend)
      1. `cd_pipeline_backend_trigger pattern` 作成
      1. `cd_pipeline_backend_trigger pattern` 適用
   1. [notifier](#notifier)
      1. `cd_pipeline_backend_trigger pattern` 作成
      1. `cd_pipeline_backend_trigger pattern` 適用
   1. [cd_pipeline_backend patternとの連携](#cd_pipeline_backend-patternとの連携)

## cd_pipeline_backend_trigger patternについて

`cd_pipeline_backend_trigger` patternは、バックエンドアプリケーションをデプロイするトリガーとなるCloudWatchイベントを構築するものです。

[cd_pipeline_backend_trigger pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/)

Eponaのドキュメントを用いて、以下について解説していきます。

* 概要
* 想定する適用対象環境
* 前提事項
* patternの適用順序

特にpatternには適用順序があることを覚えておいてください。

> 1. Delivery環境に対するcd_pipeline_backend_trigger patternの適用
> 2. Runtime環境に対するcd_pipeline_backend patternの適用
> 3. 2.で作成したリソースのARNをパラメータに設定し、Delivery環境へcd_pipeline_backend_trigger patternを再適用

:information_source:
CDパイプラインは、クロスアカウントでデプロイをする箇所であるため、各環境で相互に適用が必要になります。

### 依存しているパターン

`cd_pipeline_backend_trigger pattern` は `ci_pipeline pattern` に依存しています。

|依存パターン名|依存しているリソース|
|:---|:---|
|`ci_pipeline pattern`|コンテナイメージを格納したコンテナレジストリ|

[依存するpattern](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/#%E4%BE%9D%E5%AD%98%E3%81%99%E3%82%8Bpattern)

Delivery環境のCIパイプラインの構築については [こちら](./ci_pipeline.md) にて対応済みです[^1]。

![cd_pipeline_backend_trigger patternとの連携 pattern 依存関係](../resources/cd_pipeline_backend_trigger_dependencies.jpg)

### 適用後の構成図

上記環境から `cd_pipeline_backend_trigger pattern` を適用した場合の構成図です[^1]。

![cd_pipeline_backend_trigger pattern 適用後構成](../resources/cd_pipeline_backend_trigger_after_apply.jpg)

---

## 事前準備

### 実行ユーザーの設定

Delivery環境にリソースを作成するため、Deliveryの実行ユーザーに切り替えます。  
お好みのコマンドラインツールを開いて、以下を参考に環境変数を設定してください。

[AWS CLI を設定するための環境変数](../manual/aws_cli_configure_envvars.md)

上記設定が無事成功しているかを確認していきます。  
`$ aws sts get-caller-identity --query Arn --output text` を実行してください。

以下のように出力されたら成功です。  
※ `[LastName]` は事前に指定されたユーザー名の値が出力されていること。

```shell script
$ aws sts get-caller-identity --query Arn --output text
arn:aws:iam::938285887320:user/[LastName]DeliveryTerraformer
```

---

## トリガー構築

Runtime環境へデプロイするために、トリガーとなるCloudWatchイベントが発火できるよう環境構築します。

### ゴール

* Terraformでデプロイに必要なファイルを配置するAmazon S3バケットを構築する
* Terraformでイベントを発火させるルールを構築する
  * トリガー
    * Delivery環境のAmazon ECRへコンテナイメージがアップロード
    * Delivery環境のAmazon S3へ設定ファイルがアップロード
* 上記をAWSコンソールから確認する

### 進め方

1. Eponaのガイドにあるサンプルコードをコピーする
1. 利用するアプリケーションに合わせて、サンプルコードを修正する
1. その他、Terraformのバージョン設定など必要なファイルを作成する
1. 適用する

---

### backend

REST APIのアプリケーションであるbackendから作成していきましょう。

#### cd_pipeline_backend_trigger pattern 作成

cd_pipeline_backend_triggerパターンのbackend向けディレクトリに移動します。  
※[last_name]については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend
```

参考：ディレクトリ構成については、以下を参照ください。

[ディレクトリ構成](https://eponas.gitlab.io/epona/guide/patterns/#%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF%E3%83%88%E3%83%AA%E6%A7%8B%E6%88%90)

##### テンプレートファイルの作成

最初に、テンプレートである `main.tf` ファイルを新規作成します。  
GUIやVim, Emacsなどでファイルを作成していただいても問題ありません。

```shell script
$ touch main.tf
```

では、次にEponaのガイドに記載があるサンプルコードをコピーし、貼り付けてください。

[サンプルコード](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/#%E3%82%B5%E3%83%B3%E3%83%97%E3%83%AB%E3%82%B3%E3%83%BC%E3%83%89)

以下のようにコードを書き替えてください。

* `[last_name]` , `[LastName]` は適宜修正
* `[last_name]` 以外にも変更箇所があります。よく確認してください
  * AWS側の文字数制限によるもの
  * `handson-chat-application` のアーキテクチャによるもの（backend, notifierに分かれている等）

なお、コメントにて変更点の解説をしていますが、コメントまで記載する必要はありません。

`main.tf`

```terraform
# TODO:AWSプロバイダーを使うための設定を修正
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/[LastName]TerraformExecutionRole"
  }
}

# TODO:Runtime環境のアカウントIDに変更
locals {
  runtime_account_id = "922032444791"
}

data "aws_region" "current" {}

# TODO:区別をするため、module名を"cd_pipeline_backend_trigger_backend"に変更
module "cd_pipeline_backend_trigger_backend" {
  # TODO:参照するEponaモジュールのバージョンを記載する
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.1"

  # TODO:パイプライン名とデプロイ用設定ファイルを配置するAmazon S3バケット名を変更
  name                 = "[last_name]-pipeline-back"
  bucket_name          = "[last_name]-pipeline-back-source"
  bucket_force_destroy = true   # TODO: ハンズオンではS3バケットの削除保護を無効化するため、trueに変更
  runtime_account_id   = local.runtime_account_id

  # TODO:リポジトリ名と、その ARN およびデプロイ対象タグのマップ。ARNを記載。
  ecr_repositories = {
    "[last_name]-chat-example-backend" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["[last_name]-chat-example-backend"]
      tags = ["latest"]
    }
  }

  target_event_bus_arn      = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"
  # TODO:CodePipeline が用いる artifact store のバケット ARNを指定
  # Runtime環境上に作成するバケット名をbucket_nameとすると、arn:aws:s3:::bucket_name の形式で記載してください
  # see: https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/s3-arn-format.html
  artifact_store_bucket_arn = "arn:aws:s3:::[last_name]-back-artfct"

  # TODO: artifact_store_bucket_encryption_key_arn のパラメータをコメントアウト
  # 以下は Runtime環境上で cd_pipeline_backend_backend を動かしてから設定する
  # artifact_store_bucket_encryption_key_arn = "arn:aws:kms:${data.aws_region.current.name}:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```

##### 外部データを参照するためのファイルを作成

`main.tf` では `data` を使って、外部データを参照しようとしていることが読み取れます。　※Terraformに関する解説を思い出してください。

```terraform
  # リポジトリ名と、その ARN およびデプロイ対象タグのマップ。ARNを記載。
  ecr_repositories = {
    "[last_name]-chat-example-backend" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["[last_name]-chat-example-backend"]
      tags = ["latest"]
    }
  }
```

ここで読み込もうとしているデータは、Delivery環境のAmazon S3に配置している `terraform.tfstate` です。  
以下を実施してみてください。

`AWSコンソール` > `Amazon S3` > `[last_name]-delivery-terraform-tfstate` > `ci_pipeline`
> `terraform.tfstate` > `オブジェクトアクション` > `開く`

ここでは、どのようなリソースが作られているかを示す情報が載っています。  
`main.tf` では、ci_pipelineの `container_image_repository_arns` を読み込みたい、ということを示しています。

そこで、 `instance_dependencies.tf` ファイルを新規作成し、外部データを参照できるようにしましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch instance_dependencies.tf
```

また、以下のコードからはデータソース名とローカル名を読み取ることが出来ます。

`data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns`

* データソース名： `terraform_remote_state`
* データのローカル名： `delivery_ci_pipeline`

よって、 `instance_dependencies.tf` には、以下のように記載してください。

```terraform
data "terraform_remote_state" "delivery_ci_pipeline" {
  backend = "s3"
  config = {
    bucket         = "[last_name]-delivery-terraform-tfstate"
    key            = "ci_pipeline/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]DeliveryTerraformBackendAccessRole"
  }
}
```

##### Terraformバージョンを指定するためのファイルを作成

では次に `versions.tf` を新規作成していきましょう。  
目的としては、以下の3点です。

* Terraformのバージョンを指定する
* aws providerのバージョンを指定する
* `cd_pipeline_backend_trigger_backend` の `terraform.tfstate` ファイルの配置設定を行う

`versions.tf` ファイルを新規作成します。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch versions.tf
```

`versions.tf` には、以下のように記載してください。

```terraform
terraform {
  # Terraform は、0.13.5 以上のバージョンを利用する
  required_version = "0.13.5"

  # 本パターンを適用するための aws provider のバージョンを指定する
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  backend "s3" {
    bucket         = "[last_name]-delivery-terraform-tfstate"
    key            = "staging/cd_pipeline_backend_trigger_backend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]DeliveryTerraformBackendAccessRole"
  }
}
```

##### モジュールの出力を行うためのファイルを作成

次は、実行後の戻り値を出力するために `outputs.tf` を新規作成していきます。
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch outputs.tf
```

`outputs.tf` には、以下のように記載してください。

```terraform
output "cross_account_access_role_arn" {
  description = "CodePipelineからのクロスアカウントアクセスに使用するRoleのARN"
  value       = module.cd_pipeline_backend_trigger_backend.cross_account_access_role_arn
}
```

本patternにて作成されたRole（Delivery環境）は、今後作成されるパイプライン（Runtime環境）が使用することになります。  
そこでRoleのARNを出力させ、後続の `cd_pipeline_backend pattern` にて値を設定することでクロスアカウントでのアクセスが可能になります[^1]。

![クロスアカウントアクセス](../resources/cd_pipeline_backend_trigger_crossaccounts.jpg)

#### cd_pipeline_backend_trigger pattern 適用

##### ワークスペースの初期化

既に移動していただいていますが、以下ディレクトリに移動していることを確認してください。

```shell script
$ cd epona-aws-handson/[last_name]/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend
```

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

##### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

入力待ちの状態となります。変更を反映してよいか聞かれていればOKです。  
※ `yes` はまだ入力しないでください。

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

反映させる内容を確認してください。

* `[last_name]` の修正が反映されていること
* `Plan: 15 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…
   + "Name"   = [last_name]-backend-cd-pipeline
…

Plan: 15 to add, 0 to change, 0 to destroy

…
```

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 15 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `cd_pipeline_backend_trigger_backend` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 15 added, 0 changed, 0 destroyed.

Outputs:
cross_account_access_role_arn = arn:aws:iam::938285887320:role/[LastName]-Pipeline-BackAccessRole
```

出力されたOutputは、ローカルでメモを取っておいてください。

---

### notifier

notifierについては、時間の都合上講師により実施しています。  
必要があれば研修後に参照してください。

<!-- markdownlint-disable no-inline-html -->
<details>
  <summary>notifier 構築手順</summary>
<!-- markdownlint-enable no-inline-html -->

notifierも基本的な手順は同じですので、説明を簡略化している部分があります。

#### cd_pipeline_backend_trigger pattern 作成

cd_pipeline_backend_triggerパターンのnotifier向けディレクトリに移動します。  
※[last_name]については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier
```

##### テンプレートファイルの作成

backendと同様、テンプレートである `main.tf` ファイルを新規作成します。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch main.tf
```

では、次にEponaのガイドに記載があるサンプルコードをコピーし、貼り付けてください。

[サンプルコード](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/#%E3%82%B5%E3%83%B3%E3%83%97%E3%83%AB%E3%82%B3%E3%83%BC%E3%83%89)

以下のようにコードを書き替えてください。 `[last_name]` , `[LastName]` は適宜修正してください。  
なお、コメントにて変更点の解説をしていますが、コメントまで記載する必要はありません。

```terraform
# TODO:AWSプロバイダーを使うための設定を修正
provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::938285887320:role/[LastName]TerraformExecutionRole"
  }
}

# TODO:Runtime環境のアカウントIDに変更
locals {
  runtime_account_id = "922032444791"
}

data "aws_region" "current" {}

# TODO:module名を"cd_pipeline_backend_trigger_notifier"に変更
module "cd_pipeline_backend_trigger_notifier" {
  # TODO:参照するEponaモジュールのバージョンを記載する
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.1"

  # TODO:パイプライン名とデプロイ用設定ファイルを配置するAmazon S3バケット名を変更
  name                 = "[last_name]-pipeline-ntfr"
  bucket_name          = "[last_name]-pipeline-ntfr-source"
  bucket_force_destroy = false
  runtime_account_id   = local.runtime_account_id

  # TODO:リポジトリ名と、その ARN およびデプロイ対象タグのマップ。ARNを記載。
  ecr_repositories = {
    "[last_name]-chat-example-ntfr" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["[last_name]-chat-example-ntfr"]
      tags = ["latest"]
    }
  }

  # TODO:CodePipeline が用いる artifact store のバケット ARN
  # Runtime環境上に作成するバケット名をbucket_nameとすると、arn:aws:s3:::bucket_name の形式で記載してください
  # see: https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/s3-arn-format.html
  artifact_store_bucket_arn = "arn:aws:s3:::[last_name]-ntfr-artfct"
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # TODO:以下は Runtime環境上で cd_pipeline_backend pattern を動かしてから設定する
  # artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```

##### 外部データを参照するためのファイルを作成

`instance_dependencies.tf` ファイルを新規作成し、外部データを参照できるようにしましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch instance_dependencies.tf
```

`instance_dependencies.tf` には、以下のように記載してください。

```terraform
data "terraform_remote_state" "delivery_ci_pipeline" {
  backend = "s3"
  config = {
    bucket         = "[last_name]-delivery-terraform-tfstate"
    key            = "ci_pipeline/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]DeliveryTerraformBackendAccessRole"
  }
}
```

##### Terraformバージョンを指定するためのファイルを作成

では次にTerraformのバージョンを指定するために、 `versions.tf` を新規作成していきましょう。  
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch versions.tf
```

`versions.tf` には、以下のように記載してください。

```terraform
terraform {
  required_version = "0.13.5"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.18.0"
    }
  }

  backend "s3" {
    bucket         = "[last_name]-delivery-terraform-tfstate"
    key            = "staging/cd_pipeline_backend_trigger_notifier/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[last_name]_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::938285887320:role/[LastName]DeliveryTerraformBackendAccessRole"
  }
}
```

##### モジュールの出力を行うためのファイルを作成

次は、実行後の戻り値を出力するために `outputs.tf` を新規作成していきます。
お気に入りのエディタでファイルを作成していただいても問題ありません。

```shell script
$ touch outputs.tf
```

`outputs.tf` には、以下のように記載してください。

```terraform
output "cross_account_access_role_arn" {
  description = "CodePipelineからのクロスアカウントアクセスに使用するRoleのARN"
  value       = module.cd_pipeline_backend_trigger_notifier.cross_account_access_role_arn
}
```

#### cd_pipeline_backend_trigger pattern 適用

##### ワークスペースの初期化

既に移動していただいていますが、以下ディレクトリに移動していることを確認してください。

```shell script
$ cd epona-aws-handson/[last_name]/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier
```

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

##### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

入力待ちの状態となります。変更を反映してよいか聞かれていればOKです。  
※ `yes` はまだ入力しないでください。

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

反映させる内容を確認してください。  
問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 15 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `cd_pipeline_backend_trigger_notifier` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 15 added, 0 changed, 0 destroyed.

Outputs:
cross_account_access_role_arn = arn:aws:iam::938285887320:role/[LastName]-Pipeline-NtfrAccessRole
```

出力されたOutputは、ローカルでメモを取っておいてください。

<!-- markdownlint-disable no-inline-html -->
</details>
<!-- markdownlint-enable no-inline-html -->

---

#### コンソール上から確認

リソースが無事作成されていることを確認してみましょう。講師の指示に従ってください。

[AWSコンソール](https://aws.amazon.com/jp/console/)

* Amazon S3
* Amazon CloudWatch Events
* Amazon EventBridge

---

### cd_pipeline_backend patternとの連携

**こちらの手順は、 `cd_pipeline_backend_backend` を適用後に実施します。**  

#### cd_pipeline_backend_trigger 修正

`cd_pipeline_backend_backend` のoutputである `key_id` をコピーしてください。  
その後、以下ファイルの`xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx` を `key_id` に書き替えてください。

書き換えを実施後、コメントアウトを外してください。

`cd_pipeline_backend_trigger_backend` の `main.tf`

```terraform
  # TODO:以下は Runtime環境上で cd_pipeline_backend pattern を動かしてから設定する
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
```

#### 実行ユーザーの切り替え

`cd_pipeline_backend_trigger pattern` はDelivery環境のリソースです。  
お好みのコマンドラインツールを開いて、Deliveryの実行ユーザーへ環境変数設定を切り替えてください。

[AWS CLI を設定するための環境変数](../manual/aws_cli_configure_envvars.md)

上記設定が無事成功しているかを確認していきます。  
`$ aws sts get-caller-identity --query Arn --output text` を実行してください。

以下のように出力されたら成功です。  
※ `[LastName]` は事前に指定されたユーザー名の値が出力されていること。

```shell script
$ aws sts get-caller-identity --query Arn --output text
arn:aws:iam::938285887320:user/[LastName]DeliveryTerraformer
```

#### terraform apply

cd_pipeline_backend_triggerパターンのbackend向けディレクトリに移動します。  
※[last_name]については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend
```

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

反映させる内容を確認してください。

* `artifact_store_bucket_encryption_key_arn` の修正が反映されていること
* `Plan: 2 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…

Plan: 2 to add, 0 to change, 0 to destroy

…
```  

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 2 added, 0 changed, 0 destroyed.` という出力があること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:
cross_account_access_role_arn = arn:aws:iam::938285887320:role/[LastName]-Pipeline-BackAccessRole
```

---

#### ECSのタスク定義をデプロイ

CDパイプラインを実施するため、タスク定義をデプロイします。

`cd_pipeline_backend` で構築したパイプラインを利用するために、前提事項の確認をします。

[前提事項](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/#%E5%89%8D%E6%8F%90%E4%BA%8B%E9%A0%85)

![前提事項](../resources/cd_pipeline_backend_conditions.jpg)

1. `public_traffic_container_service_backend` でAmazon ECSを作成しているため、対応済み
1. CIパイプラインが稼働し、イメージの保存有無を確認しているため、対応済み
1. **指定されたファイルは未格納**

![設定ファイル](../resources/cd_pipeline_backend_conditions_settings.jpg)
[^1]

よって、Amazon S3に以下を手動で格納する必要があります。

* Amazon ECSのタスク定義： `taskdef.json`
* アプリケーション仕様ファイル: `appspec.yaml`

これらファイルは、既にこちらで作成済みのものを提供します。  
Eponaの使い方ではなく、AWSやアプリケーションの設定をどうするかという観点となるため、解説は割愛します。

ファイルは以下のディレクトリに格納されています。

```text
epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_backend/deploy-settings
epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_notifier/deploy-settings
```

※実PJではアプリケーションに応じてファイルを作成してください。

##### 対象ファイルをzipにまとめる

対象ファイルをzipにまとめます。

:warning: deploy-settingsディレクトリをzip化するのではなく、2つのファイルを選択し、zip化してください。

* 対象ファイル
  * appspec.yaml
  * taskdef.json
* ファイル名： settings.zip

zipファイルの作成方法は以下です。

* Windowsの場合：[ファイルを圧縮および展開する](https://support.microsoft.com/ja-jp/windows/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E3%82%92%E5%9C%A7%E7%B8%AE%E3%81%8A%E3%82%88%E3%81%B3%E5%B1%95%E9%96%8B%E3%81%99%E3%82%8B-8d28fa72-f2f9-712f-67df-f80cf89fd4e5)
* Mac OSの場合：`[上記2ファイルを選択] -> [圧縮]`

zipコマンドが利用できる場合、以下のコマンドでもzipファイルを作成できます。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/cd_pipeline_backend_backend/deploy-settings
$ zip settings.zip ./*

# 同様に、cd_pipeline_backend_notifier/deploy-settingsのファイルもzipファイル化してください
```

##### 設定ファイルのアップロード

Delivery環境のAWSコンソールから、S3バケットにタスク定義を格納したzipファイルをアップロードします。

Delivery環境のAWSコンソールから、Amazon S3サービスを開いてください。  
以下のバケットを選択し、ファイルのアップロードを実施してください。

:warning: backend用のファイルとnotifier用のファイルを混同しないように注意してください。

|対象                                                                             |バケット名                          |
|:--------------------------------------------------------------------------------|:---------------------------------|
|cd_pipeline_backend_backend/deploy-settings/settings.zipをアップロードするS3バケット |`[last_name]-pipeline-back-source`|
|cd_pipeline_backend_notifier/deploy-settings/settings.zipをアップロードするS3バケット|`[last_name]-pipeline-ntfr-source`|

アップロードできたら、Runtime環境のAWS CodeDeployからパイプラインを確認してみましょう。  
すべてグリーンになっていたら、Amazon ECSまでのデプロイが成功しています。  
失敗している方は講師に確認してもらってください。

#### 動作確認をする

デプロイが成功したら、[README](../../README.md#アプリケーションを触ってみる)の手順にしたがって、動作確認をしてみましょう。
